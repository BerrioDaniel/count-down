import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:time/Time.dart';

void main() {
  testWidgets('Renders the Time widget', (WidgetTester tester) async {
    // Build our app and trigger a frame.
    Widget testWidget = new MediaQuery(
      data: new MediaQueryData(),
      child: new MaterialApp(home: Time()),
    );
    await tester.pumpWidget(testWidget);
    await tester.pumpAndSettle(Duration(seconds: 1));

    final textTime = find.byKey(Key("textTime"));

    print(textTime);

    // Verify that the widget exists
    expect(textTime, findsWidgets);
  });
}

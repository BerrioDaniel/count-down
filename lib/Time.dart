import "package:flutter/material.dart";
import 'dart:async';
import 'package:flutter/foundation.dart';

class Time extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _Time();
}

/// This class executes a function given a time. Basically,
/// it represents a simple count down.
class _Time extends State<Time> {
  final GlobalKey<ScaffoldState> _scaffoldKeyAlert =
      new GlobalKey<ScaffoldState>();
  var toRest = new DateTime.utc(2020, DateTime.june, 1, 8);
  String day = "", hour = "", minute = "", second = "";
  Timer _timer;

  String numberFormat(int num) {
    return num >= 10 ? num.toString() : "0" + num.toString();
  }

  timeRightNow(int secondsTime) {
    try {
      _timer = Timer.periodic(new Duration(seconds: secondsTime), (timer) {
        var time = new DateTime.now();
        setState(() {
          day = numberFormat(toRest.difference(time).inDays);
          hour = numberFormat(
              int.parse(toRest.difference(time).inHours.toString()) %
                  int.parse(toRest.difference(time).inDays.toString()));
          minute = numberFormat(
              int.parse(toRest.difference(time).inMinutes.toString()) %
                  int.parse(toRest.difference(time).inHours.toString()));
          second = numberFormat(
              int.parse(toRest.difference(time).inSeconds.toString()) %
                  int.parse(toRest.difference(time).inMinutes.toString()));
        });
      });
    } catch (e) {
      _scaffoldKeyAlert.currentState.showSnackBar(
        SnackBar(
          content: Text("Error ${e.toString()} :("),
          duration: Duration(seconds: 4),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              top: Radius.circular(20.0),
            ),
          ),
        ),
      );
    }
  }

  @override
  void initState() {
    super.initState();
    timeRightNow(1);
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKeyAlert,
      appBar: new AppBar(
        elevation: 1.0,
        centerTitle: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(25.0),
          ),
        ),
        title: new Text(
          "Dias restantes",
          style: new TextStyle(
            fontFamily: "Minim",
            fontSize: 25.0,
          ),
        ),
      ),
      body: new Stack(
        children: <Widget>[
          new Container(
            width: MediaQuery.of(context).size.width,
            height: 120.0,
            child: new Center(
              child: new Text(
                "$day:$hour:$minute:$second",
                key: Key("textTime"),
                style: new TextStyle(
                  fontSize: 45.0,
                  fontFamily: "Orbitron",
                ),
              ),
            ),
          ),
          int.parse(day) < 0
              ? Center(
                  child: Text(
                    "Vacaciones",
                    style: TextStyle(
                      fontSize: 50.0,
                      fontFamily: "Orbitron",
                      color: Colors.black,
                    ),
                  ),
                )
              : SizedBox.shrink()
        ],
      ),
    );
  }
}
